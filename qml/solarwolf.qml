import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow {

  allowedOrientations: Orientation.LandscapeInverted
  property int baseDim: Theme.iconSizeSmall * Screen.widthRatio
  SystemPalette { id: activePalette }
  initialPage: Qt.resolvedUrl("pages/MainMenu.qml")
}
