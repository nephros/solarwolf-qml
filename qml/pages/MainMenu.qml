import QtQuick 2.0
import Sailfish.Silica 1.0

import "../js/solarwolf.js" as Solarwolf
import "../js/levels.js" as Levels

Dialog {
    id: mainmenu
    anchors.fill: parent
    anchors.centerIn: parent
    property string nextState: ""
    //z: 10
    //color: "black"

    Column {
        spacing: Theme.paddingMedium
        //spacing: 30
        anchors.horizontalCenter: parent.horizontalCenter
        width: ( parent.width*0.75 ) - Theme.horizontalPageMargin
        height: parent.height

        Image {
            source: "../data/logo.png"
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
            width: Theme.iconSizeExtraLarge
            //width: parent.width
            height: parent.height / 4
        }

        Button {
            id: newgamebtn
            text: "New Game"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
			  nextState = "Level-0"
			  pageStack.clear()
			  pageStack.push(Qt.resolvedUrl("Game.qml"))
			}
        }

        Button {
            id: aboutbtn
            text: "About"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
        }
    }
}
