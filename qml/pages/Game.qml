import QtQuick 2.0
import Sailfish.Silica 1.0
import "../js/solarwolf.js" as Solarwolf
import "../js/levels.js" as Levels

Page {
      id: game

      Item {
		anchors.verticalCenter: parent.verticalCenter
		anchors.left: parent.right
		Button {
		  id: leftb
		  text: "left"
		}
	  }
      Component.onCompleted: { Solarwolf.init(Levels.levels); updateTimer.start() }
      Rectangle {

      Component.onCompleted: { Solarwolf.init(Levels.levels); updateTimer.start() }
          id: screen
          //property int baseDim: 32

          color: "black"
      Timer {
          id: updateTimer
          interval: 30; repeat: true;
          onTriggered: {
			//console.log("triggered!")
			Solarwolf.update();
		  }
		  //triggeredOnStart: true
      }
          //Component.onCompleted: {  updateTimer.start() }
          //width: baseDim*15; height: baseDim*14
          width:  Math.min(parent.width, parent.height) + Theme.horizontalPageMargin * 4
          height: Math.min(parent.width, parent.height) - Theme.horizontalPageMargin
          anchors.centerIn: parent
          //rotation: -90


//          MouseArea {
//              width: screen.width; height: screen.height
//              //anchors { top: parent.top; bottom: parent.bottom }
//              anchors.centerIn: parent
//          }

          Enemy {
              id: enemy_top
              x: parseInt(Math.max(baseDim, Math.random()*screen.width))
              width: baseDim
              height: baseDim
          }

          Enemy {
              id: enemy_right
              anchors.right: screen.right
              y: parseInt(Math.max(baseDim, Math.random()*screen.height))
              width: baseDim
              height: baseDim
              rotation: 90
          }

          Enemy {
              id: enemy_bottom
              anchors.bottom: screen.bottom
              x: parseInt(Math.max(baseDim, Math.random()*screen.width))
              width: baseDim
              height: baseDim
              rotation: 180
          }

          Enemy {
              id: enemy_left
              anchors.left: screen.left
              y: parseInt(Math.max(baseDim, Math.random()*screen.height))
              width: baseDim
              height: baseDim
              rotation: -90
          }

          Ship {
              id: the_ship
              //width: baseDim - 6
              //height: baseDim - 6
              width: baseDim / 2 ; height: baseDim / 2
          }

          Timer {
              id: pause_timer
              interval: 2000
              onTriggered: Solarwolf.unpause();
          }

          Text {
              id: notification
              state: "hidden"
              anchors.right: parent.right
              font.pointSize: 24
              color: Qt.rgba(57, 146, 155, 1.0)
              Timer {
                  id: notification_timer
                  interval: 2000
                  onTriggered: Solarwolf.hideNotification()
              }

              states: [
                  State {
                      name: "hidden"
                      PropertyChanges { target: notification; visible: false; opacity: 0 }
                  },
                  State {
                      name: "visible"
                      PropertyChanges { target: notification; visible: true; opacity: 1 }
                  }
              ]

              transitions: Transition {
                  NumberAnimation { properties: "opacity"; duration: 500 }
              }
          }

          Keys.onEscapePressed: Solarwolf.escape(event)
      } //Rectangle
}//Page
