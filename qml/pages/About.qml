import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: about
	canNavigateForward: false
    anchors.fill: parent
    //z: 10
    //color: "#000045"
    property string nextState: ""

    Column {
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Text {
            text: "Created by"
            //color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            //font.pointSize: 12
        }

        Text {
            id: myname
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Nikhil Marathe"
            //color: "white"
            //font.pointSize: 20
        }

        Text {
            text: "Based on Solarwolf by Pete Shinners"
            //color: "white"
            //font.pointSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
        }

        Text {
            text: "nsm.nikhil@gmail.com"
            //color: "white"
            //font.pointSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
		    text: "SailfishOS/Silica port by"
            anchors.horizontalCenter: parent.horizontalCenter
		}
        Text {
		    text: "nephros"
            anchors.horizontalCenter: parent.horizontalCenter
		}
    }
}
